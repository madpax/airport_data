<!-- Notes
sourced from https://www.forbes.com/sites/johnnyjet/2017/11/27/how-to-make-the-most-of-southwest-airlines-cancellation-and-change-policy/
-->

# Change fee comparison

| ***Airline***|***Ticket Change Fee***|***Same Day Change Fee***|
|---|---|---|
|Alaska 	| $125 	                                        | $25 |
|American 	| $200 	                                        | $75 |
|Delta   	| $200 (up to $450 on some international fares) | $50 |
|Frontier 	| $50 - $100                                    | $25 - $50 |
|Jet Blue 	| $75 - $150                                    | $50|
|United 	| $200                                          | $75|
|Southwest 	| Free 	                                        | None - but fare difference may apply|
