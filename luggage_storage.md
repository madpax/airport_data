| ***airport code*** | ***time per unit*** | ***price per bag per unit time*** | ***size constraints*** | ***manned storage?*** | ***notes*** |
|---|---|---|---|---|---|
| BCN | 2 hours | €6 || yes | [source](https://www.aeropuertobarcelona-elprat.com/ingl/consignas-aeropuerto.htm).  Note that desk clerk for "Excess Baggage Company" denied existence of this short-term option, and also denies existence of other storage options in the airport. |
| BCN | 24 hours | €10 || yes | [source](https://www.aeropuertobarcelona-elprat.com/ingl/consignas-aeropuerto.htm) |
| BCN | 24 hours | €3,50-4,50 ||| [source](https://www.barcelona.com/barcelona_city_guide/all_about/barcelona_airport_bcn/airport_faq) |
| JFK | 24 hours | $8 | up to 20 inches high || [source](https://www.reddit.com/r/Flights/comments/coxqf4/airport_luggage_storage_price_gouged_at_bcn/ewmgt0p/) |
| JFK | 24 hours | $12 | 21-24 inches high || [source](https://www.reddit.com/r/Flights/comments/coxqf4/airport_luggage_storage_price_gouged_at_bcn/ewmgt0p/) |
| JFK | 24 hours | $16 | 25-26 inches high || [source](https://www.reddit.com/r/Flights/comments/coxqf4/airport_luggage_storage_price_gouged_at_bcn/ewmgt0p/) |
| JFK | 24 hours | $20 | 27-30 inches high || airport website [untrustworthy](https://www.reddit.com/r/Flights/comments/coxqf4/airport_luggage_storage_price_gouged_at_bcn/ewm53rh/), at least one point in time |
| JFK | 24 hours | $24 | over 30 inches high || [source](https://www.reddit.com/r/Flights/comments/coxqf4/airport_luggage_storage_price_gouged_at_bcn/ewmgt0p/) |
